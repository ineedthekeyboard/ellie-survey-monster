class Ellie extends Backbone.View

  el: '#ellieContainer'

  state: 'none'

  question: 1

  answers: []

  ellieTalkingSpeed: -250000

  ellieStartDelay: 800

  ellieBackSpeed: -3000

  ellieBackDelay: 800

  events: {
    'click .readyBtn': 'question1'
    'click #start .notReadyBtn': 'waitingOnUserConversation'
    'click #questionCollector a': 'answerHandler'
  }

  initialize: ()->
    $('.loadingSpinner').fadeOut('fast')
    setTimeout((() => @$el.fadeIn('fast'); @startOffConversation()), 500)

  render: ()->
    ###Primarily controls what answers are available to the user to select###
    if (@state == 'none')
      @startOffConversation()
    else if (@state == 'start')
      @$el.find('#start').fadeIn('fast')
    else if (@state == 'wait')
      @$el.find('#whenReadyQuestion').fadeIn('fast')
    else if (@state == 'question1' || @state == 'question2' || @state == 'question3')
      @$el.find('#questionCollector').fadeIn('fast')


  question1: ()->
    @standardTransition()
    @state = 'question1'
    @question = 1
    question1 = [
      "Alright Lets Get Started.",
      "^500 Let me first explain how this will work.",
      "I am going to ask you a few personal questions. <br> To get to know you. <br>",
      "All you have to do is tell me how strongly you agree or disagree with me. Also, ^500I strongly urge you to be honest,^400 and answer every question.^400",
      "To answer each question use the buttons to indicate your feelings.^2000."
      "Here is the first question^200.^200.^200.",
      "When you find your Email Inbox full of unread mail,^300 <Br>as mine always seems to be,^300 <br> you just can not answer any emails until it's organized."
    ]
    @ellieTalks(question1)

  question2: ()->
    @standardTransition()
    @state = 'question2'
    @question = 2
    question2 = [
      "Whew wasn't that one easy? ^500 <br> Question No 2 comming right up. ^500",
      "When I am out on a spring walk, my surroudings can just disappear to my inner thoughts.<br>Do you know this feeling?"
    ]
    @ellieTalks(question2)

  question3: ()->
    @standardTransition()
    @state = 'question3'
    @question = 3
    if @answers[2].strength == 1 || @answers[2].strength == 2
      varAnswer = 'Yeah I am just never dreamy either.'
    else if @answers[2].strength == 3 || @answers[2].strength == 4
      varAnswer = 'You know, ^300 I often find hours disappear when I day dream.'
    else
      varAnswer = 'Hmmm. Interesting'

    question3 = [
      "" + varAnswer,
      "Moving right along aren't we.^300 Next One..",
      "Everyone needs a change sometimes,<br> ^300 but you are someone that craves change all the time."
    ]
    @ellieTalks(question3)

  waitingOnUserConversation: ()->
    @standardTransition()
    @state = 'wait'
    waitingOnConvo = [
      "Oh, your not ready to continue yet?",
      "That's alright, I am more than happy to wait until your ready.",
      "Just let me know when your ready to get started!"
    ]
    @ellieTalks(waitingOnConvo)

  startOffConversation: ()->
    @state = 'start'
    startOffConvo = [
      "Hi,^1000 I am Ellie. ^1000 <br>It's a Pleasure to See You Here.",
      "I am looking forward to learning more about you!^1000 <br>",
      "If your ready let's get started."
    ]
    @ellieTalks(startOffConvo)

  standardTransition: ()->
    @$el.find('#whenReadyQuestion').fadeOut('fast')
    @$el.find('.ellie').fadeOut('fast')
    @$el.find('#start').fadeOut('fast')
    @$el.find('#questionCollector').fadeOut('fast')

  ellieTalks: (ellieSpeech)->
    ###Reset Ellie then call her to speak again###
    @$el.find('.ellie').remove()
    @$el.find('.ellieHolder').append('<h3 class="ellie setText text-center"></h3>')
    @$el.find(".ellie").typed({
      strings: ellieSpeech
      typeSpeed: @ellieTalkingSpeed
      startDelay: @ellieStartDelay
      showCursor: false
      cursorChar: "|"
      backSpeed: @ellieBackSpeed
      backDelay: @ellieBackDelay
      preStringTyped: ->
        $('.double-bounce2').show()
      callback: =>
        setTimeout((()-> $('.double-bounce2').hide()), 500)
        @render()
    })

  answerHandler: (e) ->
    questionAnswer = $(e.currentTarget).data('strength')
    answer = {
      strength: questionAnswer,
      questionNum: @question,
      computedAnswer: @randomNum(1, 3.141) * questionAnswer
    }
    @answers[@question] = answer
    console.log @answers
    console.log @state
    if (@state == 'question1')
      @question2()
      return false
    else if (@state == 'question2' && @question == 2)
      @question3()
      return false
    else if (@state == 'question3' && @question == 3)
      @endExam()
      return false


  endExam: ()->
    @standardTransition()
    @state = 'finalReview'
    startOffConvo = [
      "Well done! Lets take a look at the results...<br> This Chart shows your real and adjusted quotionents."
    ]
    @ellieTalks(startOffConvo)
    @renderChart()

  renderChart: ()->
    data = {
      labels: ["IQ", "EQ", "XQ"],
      datasets: [
        {
          label: "Your Life Skillz",
          fillColor: "rgba(220,220,220,0.2)",
          strokeColor: "rgba(220,220,220,1)",
          pointColor: "rgba(220,220,220,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: [@answers[1].computedAnswer, @answers[2].computedAnswer, @answers[3].computedAnswer]
        },
        {
          label: "Your Mental Skillz",
          fillColor: "rgba(151,187,205,0.2)",
          strokeColor: "rgba(151,187,205,1)",
          pointColor: "rgba(151,187,205,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(151,187,205,1)",
          data: [@answers[1].strength, @answers[2].strength, @answers[3].strength]
        }
      ]
    }
    @ctx = $("#myChart").get(0).getContext("2d")
    @myRadarChart = new Chart(@ctx).Radar(data, { pointLabelFontFamily : "'Arial'"});

  randomNum: (min, max) ->
    return Math.floor(Math.random() * (max - min + 1) + min);


$ ->
#create utility function for toasting...
  window.displayAlert = (message) ->
    alertToast = '<div id="mainAlert1" data-alert class="alert-box alert round" style="display:none;margin-top:25px;" tabindex="0" aria-live="assertive" role="dialogalert">' + message.toString() + '<a href="#" tabindex="0" class="close" aria-label="Close Alert">&times;</a></div>'
    $('.panel').prepend(alertToast)
    $(document).foundation('alert', 'reflow')
    $('#mainAlert1').fadeIn('slow')
    setTimeout((() -> $('#mainAlert1 .close').trigger('click')), 4000)
  #Bootstrap the application
  window.Ellie = new Ellie({})
  #load foundation for toasts...
  $(document).foundation()


